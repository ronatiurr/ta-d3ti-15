-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2020 at 03:32 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homestay`
--

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(16),
(16),
(16);

-- --------------------------------------------------------

--
-- Table structure for table `homestay`
--

CREATE TABLE `homestay` (
  `id_homestay` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `qty` int(9) NOT NULL,
  `keterangan` varchar(500) NOT NULL,
  `gambar` varchar(250) NOT NULL,
  `id_kec` int(11) NOT NULL,
  `fasilitas` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `poi` varchar(500) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homestay`
--

INSERT INTO `homestay` (`id_homestay`, `nama`, `qty`, `keterangan`, `gambar`, `id_kec`, `fasilitas`, `alamat`, `poi`, `status`) VALUES
(6, 'Blessing Homestay', 2, 'Lokasi strategis, dekat pantai, cocok digunakan untuk acara keluarga', '', 2, 'Kamar mandi dalam ', 'Pantai Bul-Bul', 'Pantai Lumban Bul bul 50 m', ''),
(43, 'Homestay Jos N Mo', 3, 'Homestay Jos N Mo merupakah homestay dengan harga yang murah,  nyaman seperti di rumah sendiri dan interaksi budaya kekeluargaan', '', 2, 'Kamar mandi dalam', 'Jl. Sutomo No. 15', 'Depan kantor camat\r\n500 m sekolah unggulan Yasop\r\n400 m makam Sisingamangaraja\r\n 500 m Museum Batak TB Center', ''),
(45, 'Mikha Homestay', 3, 'Yang 5L mari datang ke homestay kami, cocok digunakan untuk keluarga', 'https://firebasestorage.googleapis.com/v0/b/pemilik-app.appspot.com/o/images%2FMikha%20Homestay.jpg?alt=media&token=a48d2bc4-49b7-488b-a05f-5db1a33325d4%27', 2, 'Kamar Mandi dalam', 'jl.guru somalaing hauma bange kelurahan lumban dolok', 'Pantai Lumban Bul-bul', ''),
(46, 'Nauli Homestay', 2, 'Posisi Strategis di tengah kota Balige\r\n Bersih , aman dan nyaman', 'https://firebasestorage.googleapis.com/v0/b/pemilik-app.appspot.com/o/images%2FNauli%20Homestay.jpg?alt=media&token=ec9f8e87-a7b8-45dc-b194-0a93beb4461b%27', 1, 'Kamar Mandi Luar\r\nTV', 'Soposurung', '400 Meter Makam Sisingamangaraja\r\n500 Meter Museum Batak TB Silalahi', ''),
(47, 'Aster Homestay', 2, 'Aman, nyaman, strategis, dan cocok digunakan keluarga', 'https://firebasestorage.googleapis.com/v0/b/pemilik-app.appspot.com/o/images%2FAster%20Homestay.jpg?alt=media&token=206700e2-ced8-4001-b415-68c073010ce1%27', 2, 'Kamar Mandi luar', 'Pantai Lumban Bul-bul', 'Pantai Lumban Bul-bul 50 m', ''),
(48, 'Ita Pagar Batu Homestay', 6, 'Memiliki Fasilitas lengkap, lingkungan asri dan nyaman', 'https://firebasestorage.googleapis.com/v0/b/pemilik-app.appspot.com/o/images%2FIta%20pagar%20batu%20homestay.jpg?alt=media&token=5dd7a185-826e-4447-882d-66f2ab1bad87%27', 2, '1. Free bikes\r\n2. Garden\r\n3. Terrace\r\n4. Unit Feature Tv\r\n5. Private Bathroom and shower\r\n6. Kitchen', 'Jalan DR TB Silalahi Pagar Batu', '1. Museum Batak TB Silalahi Center 0,1 KM\r\n2. Ita Soposurung Hotel & Resto 0,5 KM\r\n3. Alfa Midi 0,6 KM\r\n4. Cafe Bunga Toba 0,8 KM\r\n5. Balerong Balige Traditional Market 1,5 KM\r\n6. Sibodiala Circuit 3,5 KM\r\n7, Dolok Tolong 5 Km\r\n8. Silangit International Airport 10,3 KM', ''),
(50, 'Mawar Homestay', 2, 'Aman, Nyaman, Lingkungan yang indah', 'https://firebasestorage.googleapis.com/v0/b/pemilik-app.appspot.com/o/images%2FMawar%20Homestay.jpg?alt=media&token=ea3ae077-b308-450e-80f3-67f1006f97e1%27', 2, 'Kamar mandi luar 1', 'Pantai Bul-bul', '1. Pantai Lumban Bul-bul 100 M\r\n2. Onan Balerong 1 KM\r\n', ''),
(51, 'Evelin Homestay', 3, 'Aman, nyaman, dan lingkungan asri', 'https://firebasestorage.googleapis.com/v0/b/pemilik-app.appspot.com/o/images%2FEfelyn%20Homestay.jpg?alt=media&token=a3671acf-bb4d-4836-980c-cfd82e3edeaf%27', 1, 'Kamar mandi luar', 'Pantai Lumban Bul-bul', 'Pantai Lumban Bul-bul 100 M', ''),
(52, 'Anggrek Homestay', 3, 'Aman, asri dan lingkungan indah', '', 2, 'Kamar mandi luar', 'Pantai Bul bul', '1. Pantai Lumban Bul-bul 50 m', ''),
(53, 'Teo Jose Homestay', 3, 'Aman, nyaman dan lingkungan asri', '', 2, 'Kamar mandi luar 1', 'Pantai Bul-bul', '1. Pantai Bul-bul 50M\r\n2. Museum Batak', ''),
(54, 'Martogi Homestay', 3, 'Aman, nyaman dan strategis', 'https://firebasestorage.googleapis.com/v0/b/pemilik-app.appspot.com/o/images%2FMartogi%20Homestay.jpg?alt=media&token=14f46718-5615-414a-a42f-8619dd1b184d%27', 2, 'Kamar mandi luar 1', 'Pantai Bul bul', 'Pantai Bul bul 50 M', '');

-- --------------------------------------------------------

--
-- Table structure for table `kamar`
--

CREATE TABLE `kamar` (
  `no_kamar` int(11) NOT NULL,
  `harga_kamar` varchar(100) NOT NULL,
  `value` int(100) NOT NULL,
  `id_homestay` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kamar`
--

INSERT INTO `kamar` (`no_kamar`, `harga_kamar`, `value`, `id_homestay`, `jumlah`) VALUES
(1, '585300', 1, 48, 1),
(2, '1174600', 2, 48, 1),
(3, '20000', 1, 50, 1),
(4, '200000', 2, 50, 1),
(5, '200000', 1, 6, 1),
(6, '200000', 2, 6, 1),
(7, '200000', 1, 43, 1),
(8, '200000', 2, 43, 2),
(10, '250000', 1, 45, 1),
(11, '250000', 2, 45, 2),
(13, '200000', 1, 46, 1),
(14, '200000', 2, 46, 1),
(15, '200000', 1, 47, 1),
(16, '2000000', 2, 47, 1),
(17, '200000', 1, 51, 1),
(18, '200000', 2, 51, 1),
(20, '200000', 1, 52, 1),
(21, '200000', 2, 52, 2),
(23, '200000', 1, 53, 1),
(24, '200000', 2, 53, 2),
(26, '200000', 0, 54, 0),
(27, '200000', 0, 54, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id_kec` int(11) NOT NULL,
  `nama_kec` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id_kec`, `nama_kec`) VALUES
(1, 'Ajibata'),
(2, 'Balige'),
(3, 'Bonatua Lunasi'),
(4, 'Borbor'),
(5, 'Habinsaran'),
(6, 'Laguboti'),
(7, 'Lumban Julu'),
(8, 'Nassau'),
(9, 'Parmaksian'),
(10, 'Pintu pohan'),
(11, 'Porsea'),
(12, 'Siantar Narumonda'),
(13, 'Sigumpar'),
(14, 'Silaen'),
(15, 'Tampahan'),
(16, 'Uluan');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `id_pemesanan` int(11) NOT NULL,
  `bukti_pembayaran` varchar(200) NOT NULL,
  `type_pembayaran` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan`
--

CREATE TABLE `pemesanan` (
  `id_pemesanan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_homestay` int(11) NOT NULL,
  `no_kamar` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jumlah_tamu` int(11) NOT NULL,
  `room_type` varchar(100) NOT NULL,
  `check_in` datetime NOT NULL,
  `check_out` datetime NOT NULL,
  `total_price` int(11) DEFAULT NULL,
  `keterangan_pengunjung` varchar(100) NOT NULL,
  `bukti_bayar` varchar(250) NOT NULL,
  `metode_bayar` varchar(100) NOT NULL,
  `status_transaksi` varchar(100) NOT NULL,
  `jumlah_kamar` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemesanan`
--

INSERT INTO `pemesanan` (`id_pemesanan`, `id_user`, `id_homestay`, `no_kamar`, `nama`, `jumlah_tamu`, `room_type`, `check_in`, `check_out`, `total_price`, `keterangan_pengunjung`, `bukti_bayar`, `metode_bayar`, `status_transaksi`, `jumlah_kamar`) VALUES
(1, 14, 52, 5, 'Ronatiur Febriani LumbanGaol', 4, 'Exclusive', '2020-06-22 00:00:00', '2020-06-23 00:00:00', 2500000, 'sehatt', 'marlina.jpg', 'Bayar DP', 'Lunas', 3),
(2, 14, 48, 14, 'Marlina Simangunsong', 4, 'Exclusive', '2020-06-22 00:00:00', '2020-07-03 00:00:00', 2500000, 'sakit', '.JPG', 'Bayar DP', 'Lunas', NULL),
(3, 1, 45, 7, 'Ronatiur Febriani Lumban Gaol', 4, 'Economi', '2020-06-22 00:00:00', '2020-07-24 00:00:00', 2500000, 'Sehat', 'rona.jpg', 'transfer', '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pendaftaran`
--

CREATE TABLE `pendaftaran` (
  `id_pendaftaran` int(11) NOT NULL,
  `nama_homestay` varchar(45) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `fasilitas` varchar(70) NOT NULL,
  `alamat` varchar(90) NOT NULL,
  `kecamatan` varchar(25) NOT NULL,
  `poi` varchar(50) NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `status` varchar(90) NOT NULL,
  `id_homestay` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendaftaran`
--

INSERT INTO `pendaftaran` (`id_pendaftaran`, `nama_homestay`, `gambar`, `fasilitas`, `alamat`, `kecamatan`, `poi`, `deskripsi`, `status`, `id_homestay`, `id_user`) VALUES
(2, 'Evelyn', '', '1.Kamar Mandi', 'Silaen', '0', '1. Lumban Bulbul 1', 'Homestay ini didirikan tahun 2019', 'Di terima', NULL, NULL),
(3, 'Melati Homestay', 'rona.jpg', '1. Kamar mandi dalam\r\n2. CCTV', 'Balige', '2', '1. Pantai Lumban Bulbul', 'Homestay ini merupakan homestay di kecamatan Balige yang memiliki keindahan alam disekitarnya', '', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id_role` int(3) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id_role`, `role`) VALUES
(1, 'Admin'),
(2, 'Pengunjung'),
(3, 'pemilik_homestay');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nohp` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `nama`, `alamat`, `email`, `nohp`, `username`, `password`, `role`) VALUES
(1, 'Ronatiur Febriani Lumban Gaol', 'jln. siliwangi doloksanggul', 'ronatiurfebriani09@gmail.com', '082277990123', 'admin', 'admin', 1),
(3, 'rona', 'sigordang', 'ronatiurfebriani09@gmail.com', '082360414241', 'rona', 'ron', 3),
(14, 'Marlina Simangunsong', 'Lumban Bul-bul', 'marlina@gmail.com', '08236041421', 'marlinaa', 'marlina', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `homestay`
--
ALTER TABLE `homestay`
  ADD PRIMARY KEY (`id_homestay`),
  ADD KEY `id_kategoriFK` (`id_kec`);

--
-- Indexes for table `kamar`
--
ALTER TABLE `kamar`
  ADD PRIMARY KEY (`no_kamar`),
  ADD KEY `kamar_ibfk_1` (`id_homestay`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id_kec`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD KEY `id_pemesanan` (`id_pemesanan`);

--
-- Indexes for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`),
  ADD KEY `id_homestay` (`id_homestay`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `no_kamar` (`no_kamar`);

--
-- Indexes for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD PRIMARY KEY (`id_pendaftaran`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_roleFK` (`role`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `homestay`
--
ALTER TABLE `homestay`
  MODIFY `id_homestay` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `kamar`
--
ALTER TABLE `kamar`
  MODIFY `no_kamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id_kec` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  MODIFY `id_pendaftaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `homestay`
--
ALTER TABLE `homestay`
  ADD CONSTRAINT `id_kategoriFK` FOREIGN KEY (`id_kec`) REFERENCES `kecamatan` (`id_kec`);

--
-- Constraints for table `kamar`
--
ALTER TABLE `kamar`
  ADD CONSTRAINT `kamar_ibfk_1` FOREIGN KEY (`id_homestay`) REFERENCES `homestay` (`id_homestay`);

--
-- Constraints for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD CONSTRAINT `pembayaran_ibfk_1` FOREIGN KEY (`id_pemesanan`) REFERENCES `pemesanan` (`id_pemesanan`);

--
-- Constraints for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD CONSTRAINT `pemesanan_ibfk_1` FOREIGN KEY (`id_homestay`) REFERENCES `homestay` (`id_homestay`),
  ADD CONSTRAINT `pemesanan_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`),
  ADD CONSTRAINT `pemesanan_ibfk_3` FOREIGN KEY (`no_kamar`) REFERENCES `kamar` (`no_kamar`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `id_roleFK` FOREIGN KEY (`role`) REFERENCES `role` (`id_role`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
