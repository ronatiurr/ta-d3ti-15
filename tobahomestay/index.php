<?php 
include 'conn.php'
 ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Toba Homestay</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="../image/logo.png" width="220"></a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"> <?php echo date('l, d-m-Y'); ?> &nbsp; <a href="../logout.php" class="btn btn-success square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
            <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <!-- <li class="text-center">
                        <img src="assets/img/find_user.png" class="user-image img-responsive"/>
                        </li> -->
                    <li>
                        <a  class="active-menu" href="index.php"><i class="fa fa-dashboard fa-3x"></i> Beranda</a>
                    </li>
                    <li>
                        <a  href="data_homestay.php"><i class="fa fa-tree fa-3x"></i> Toba Homestay</a>
                    </li>
                     <li>
                        <a  href="data_pendaftaran_homestay.php"><i class="fa fa-folder-open fa-3x"></i> Daftar Homestay</a>
                    </li>
                      <li>
                        <a  href="data_user.php"><i class="fa fa-user fa-3x"></i> Daftar Pengguna</a>
                    </li>
                     <li>
                        <a  href="data_toba_wisata.php"><i class="fa fa-user fa-3x"></i> Pemesanan Homestay</a>
                    </li>
                  <li >
                        <a  href="blank.html"><i class="fa fa-square-o fa-3x"></i> Blank Page</a>
                    </li>	
                </ul>
               
            </div>
            
        </nav><!-- 

         body -->

         <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Selamat datang , Admin</h2>
                     <center><img src="../image/logo.png" width="100%"></center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">           
                        <div class="panel panel-back noti-box">

                            <span class="icon-box bg-color-red set-icon">
                                <i class="fa fa-user"></i>
                            </span>
                            <div class="text-box" >
                                

                                <?php 
                                $query = "SELECT COUNT(id_user) FROM users WHERE role = 3";
                                     $jumlah_pengguna = mysqli_query($conn, $query);
                                     $jumlah_penggunas = mysqli_fetch_array($jumlah_pengguna); 
                                    ?>
                                       
                                    <p class="main-text"><?=$jumlah_penggunas['COUNT(id_user)'];?> User</p><p class="text-muted">Jumlah Pengguna</p>                                    
                                
                            </div>
                        </div>
                     </div>
                     <div class="col-md-6">           
                        <div class="panel panel-back noti-box">
                            <span class="icon-box bg-color-blue set-icon">
                                <i class="fa fa-user"></i>
                            </span>
                            <div class="text-box" >
                                

                                <?php 
                                $query = "SELECT COUNT(id_pendaftaran) FROM pendaftaran";
                                     $jumlah_pengguna = mysqli_query($conn, $query);
                                     $jumlah_penggunas = mysqli_fetch_array($jumlah_pengguna); 
                                    ?>
                                    <p class="main-text"><?=$jumlah_penggunas['COUNT(id_pendaftaran)'];?> Pendaftaran</p>                                    
                                <p class="text-muted">Jumlah Pendaftaran</p>
                            </div>
                        </div>
                     </div>
                     
                 </div>
                 <div class="row">
                    <div class="col-md-6">           
                        <div class="panel panel-back noti-box">
                            <span class="icon-box bg-color-brown set-icon">
                                <i class="fa fa-list"></i>
                            </span>
                            <div class="text-box" >
                                

                                <?php 
                                $query = "SELECT COUNT(id_pemesanan) FROM pemesanan ";
                                     $jumlah_pengguna = mysqli_query($conn, $query);
                                     $jumlah_penggunas = mysqli_fetch_array($jumlah_pengguna); 
                                    ?>
                                    <p class="main-text"><?=$jumlah_penggunas['COUNT(id_pemesanan)'];?> Jumlah Pemesanan</p>                                    
                                <p class="text-muted">Pemesanan Homestay</p>
                            </div>
                        </div>
                     </div>
                     <div class="col-md-6">           
                        <div class="panel panel-back noti-box">
                            <span class="icon-box bg-color-green set-icon">
                                <i class="fa fa-list"></i>
                            </span>
                            <div class="text-box" >
                                

                                <?php 
                                $query = "SELECT COUNT(id_homestay) FROM homestay ";
                                     $jumlah_pengguna = mysqli_query($conn, $query);
                                     $jumlah_penggunas = mysqli_fetch_array($jumlah_pengguna); 
                                    ?>
                                    <p class="main-text"><?=$jumlah_penggunas['COUNT(id_homestay)'];?> Jumlah Homestay</p>                                    
                                <p class="text-muted">Toba Homestay</p>
                            </div>
                        </div>
                     </div>
                 </div>

                
                 <hr />
               
        </div>
             <!-- /. PAGE INNER  -->
    </div>
         <!-- /. PAGE WRAPPER  -->
        <!-- </div> -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
