<?php require_once("../conn.php"); ?>
<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Daftar Pemesanan | Admin Toba Homestay</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>

<style>
    #gambarGaleri{
        width: 60px;
        height: 30px;
    }
</style>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Toba Homestay</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"> <a href="../logout.php" class="btn btn-success square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
            <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                <!-- <li class="text-center">
                    <img src="assets/img/find_user.png" class="user-image img-responsive"/>
                    </li> -->
                
                      <li>
                        <a  class="active-menu" href="index.php"><i class="fa fa-dashboard fa-3x"></i> Beranda</a>
                    </li>
                    <li>
                        <a  href="data_homestay.php"><i class="fa fa-tree fa-3x"></i> Toba Homestay</a>
                    </li>
                     <li>
                        <a  href="data_pendaftaran_homestay.php"><i class="fa fa-folder-open fa-3x"></i> Daftar Homestay</a>
                    </li>
                      <li>
                        <a  href="data_user.php"><i class="fa fa-user fa-3x"></i> Daftar Pengguna</a>
                    </li>
                     <li>
                        <a  href="data_pemesanan.php"><i class="fa fa-user fa-3x"></i> Pemesanan Homestay</a>
                    </li>
                  <li >
                        <a  href="blank.html"><i class="fa fa-square-o fa-3x"></i> Blank Page</a>
                    </li>   
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <!-- <div class="row">
                    <div class="col-md-12">
                     <h2>Blank Page</h2>   
                        <h5>Welcome Jhon Deo , Love to see you back. </h5>
                       
                    </div>
                </div> -->

                <!--  -->
                <!--  -->
     <div class="container">    
                    <div class="row">
                        <div class="col-md-10">
                            <h3>Daftar Pemesanan</h3>
                             <button = style="margin-bottom:10px" onClick="print_data()" class="btn btn-default pull-right"><span class='glyphicon glyphicon-print'></span>  Cetak</button> 
                            <br>
                            <table class="table table-hover">
                                <tr>
                                    <th>id</th>
                                    <th>Nama Pemesan
                                    </th>
                                    <th>Room Type</th>
                                    <th> Jumlah Tamu</th>
                                    <th>Metode Pembayaran</th>
                                    <th>Total Harga</th>
                                    <th>Status</th>
                                    <th>Bukti Pembayaran</th>
                                    <th>Opsi</th>
                                </tr>
                                <?php 
                                $query2 = "SELECT * FROM pemesanan order by id_pemesanan desc";
                                $tobawis = mysqli_query($conn, $query2);
                                while($tobawisa = mysqli_fetch_array($tobawis)){ ?>   
                                <tr>
                                    <td><?=$tobawisa['id_pemesanan']?></td>
                                    <td><?=$tobawisa['nama']?></td>
                                    <td><?=$tobawisa['room_type']?></td>
                                    <td><?=$tobawisa['jumlah_tamu']?></td>
                                    <td><?=$tobawisa['metode_bayar']?></td>
                                    <td><?=$tobawisa['total_price']?></td>
                                    <td><?=$tobawisa['status_transaksi']?></td>
                                    <td><?=$tobawisa['bukti_bayar']?></td>
                                    <?php if($tobawisa['status_transaksi'] == "Lunas" && $tobawisa['bukti_bayar']!="belum dikirim"){?>
                                    <td style="color:green;"><i class="fa fa-check"></i>Transaksi Selesai</td>
                                <?php }else{ ?>
                                    <td><a href="beri_konfirmasi_transaksi.php?id_toba=<?=$tobawisa['id_pemesanan']?>"><button class="btn btn-primary"><i class="fa fa-location-arrow"></i>  Beri Konfirmasi</button></a></td>
                                <?php } ?>
                                </tr>
                            <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               <?php
  ?>
<script>
  function print_data() {
    <?php 
    if (isset($cari)) { ?>
      window.open("laporan_pemesanan.php?barang=<?php echo $cari?>","_blank");
    <?php }else{ ?>
      window.open("laporan_pemesanan.php","_blank");
    <?php } ?>
  }
</script>
        </div>
             <!-- /. PAGE INNER  -->
    </div>
         <!-- /. PAGE WRAPPER  -->
        <!-- </div> -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
