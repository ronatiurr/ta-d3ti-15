<?php 
include 'config.php';
 ?>
<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Daftar Homestay | Admin Toba Homestay</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>

<style>
	#gambarGaleri{
		width: 60px;
		height: 30px;
	}
</style>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Toba Homestay</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"> <a href="../logout.php" class="btn btn-success square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
            <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<!-- <li class="text-center">
                    <img src="assets/img/find_user.png" class="user-image img-responsive"/>
					</li> -->
				
					  <li>
                        <a  class="active-menu" href="index.php"><i class="fa fa-dashboard fa-3x"></i> Beranda</a>
                    </li>
                    <li>
                        <a  href="data_homestay.php"><i class="fa fa-tree fa-3x"></i> Toba Homestay</a>
                    </li>
                     <li>
                        <a  href="data_pendaftaran_homestay.php"><i class="fa fa-folder-open fa-3x"></i> Daftar Homestay</a>
                    </li>
                      <li>
                        <a  href="data_user.php"><i class="fa fa-user fa-3x"></i> Daftar Pengguna</a>
                    </li>
                     <li>
                        <a  href="data_pemesanan.php"><i class="fa fa-user fa-3x"></i> Pemesanan Homestay</a>
                    </li>
                  <li >
                        <a  href="blank.html"><i class="fa fa-square-o fa-3x"></i> Blank Page</a>
                    </li> 	
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <!-- <div class="row">
                    <div class="col-md-12">
                     <h2>Blank Page</h2>   
                        <h5>Welcome Jhon Deo , Love to see you back. </h5>
                       
                    </div>
                </div> -->

                <!--  -->
                <!--  -->
     <div class="container">
	<div class="row">
		<div class="col-sm-8">
		<h3><span class="glyphicon glyphicon-plus"></span> Tambah Homestay</h3>
<br/>
<br/>
        <form action="tmb_hmsty_act.php" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label>Nama Homestay</label>
            <input name="nama" type="text" class="form-control" placeholder="Nama Homestay ..">
          </div>
        <!--  <div class="form-group">
            <label>Harga</label>
            <input name="harga" type="text" class="form-control" placeholder="Harga Homestay ..">
          </div> -->
          <div class="form-group">
            <label>Jumlah</label>
            <input name="qty" type="text" class="form-control" placeholder="Jumlah ..">
          </div>
          <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="keterangan" type="text" class="form-control" placeholder="Deskripsi.."></textarea> 
          </div>
          <div class="form-group">
            <label>Kecamatan</label>
            <select class="form-control" name="kecamatan">
            <?php
              $query = mysqli_query($koneksi, "SELECT * FROM kecamatan");
              while($q = mysqli_fetch_assoc($query)){
              echo '<option>'. $q['nama_kec'] .'</option>';   
              }
            ?>
            </select>
          </div>
          <div class="form-group">
            <label>Gambar</label>
            <input name="gambar" type="file" class="form-control">
          </div>
          <div class="form-group">
            <label>Fasilitas</label>
            <textarea name="fasilitas" type="text" class="form-control" placeholder="Fasilitas.."></textarea>
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat" type="text" class="form-control" placeholder="Alamat.."></textarea>
          </div>
          <div class="form-group">
            <label>POI</label>
            <textarea name="poi" type="text" class="form-control" placeholder="POI.."></textarea>
          </div>
          <input type="submit" class="btn btn-primary" value="Simpan" name="tambah">
          <a href="data_homestay.php" class="btn btn-default" data-dismiss="modal">Batal</a>
      </form>
    <br><br>
  </div>

                            <!--  -->
                            <!--  -->
        

                 <!-- /. ROW  -->
    <hr />
               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
