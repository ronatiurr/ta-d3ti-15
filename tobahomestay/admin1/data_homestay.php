<?php
include 'config.php';
?>
<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Daftar Homestay | Admin Toba Homestay</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>

<style>
	#gambarGaleri{
		width: 60px;
		height: 30px;
	}
</style>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <a class="navbar-brand" href="index.php">Toba Homestay</a>
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"> <a href="../logout.php" class="btn btn-success square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
            <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<!-- <li class="text-center">
                    <img src="assets/img/find_user.png" class="user-image img-responsive"/>
					</li> -->
				
					
                   <li>
                        <a  class="active-menu" href="index.php"><i class="fa fa-dashboard fa-3x"></i> Beranda</a>
                    </li>
                    <li>
                        <a  href="data_homestay.php"><i class="fa fa-tree fa-3x"></i> Toba Homestay</a>
                    </li>
                     <li>
                        <a  href="data_pendaftaran_homestay.php"><i class="fa fa-folder-open fa-3x"></i> Daftar Homestay</a>
                    </li>
                      <li>
                        <a  href="data_user.php"><i class="fa fa-user fa-3x"></i> Daftar Pengguna</a>
                    </li>
                     <li>
                        <a  href="data_pemesanan.php"><i class="fa fa-user fa-3x"></i> Pemesanan Homestay</a>
                    </li>
                  <li >
                        <a  href="blank.html"><i class="fa fa-square-o fa-3x"></i> Blank Page</a>
                    </li> 
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <!-- <div class="row">
                    <div class="col-md-12">
                     <h2>Blank Page</h2>   
                        <h5>Welcome Jhon Deo , Love to see you back. </h5>
                       
                    </div>
                </div> -->
     <style>
	#gambarMobil{
		width:100px; 
		height:60px;
	}
</style>
  <div class="container"> 
    <div class="row">
      <div class="col-md-10">
       <h3><span class="glyphicon glyphicon-list"></span> Data Homestay</h3>
<a style="margin-bottom:20px" class="btn btn-info col-md-2" href="tambah_homestay.php"><span class="glyphicon glyphicon-plus"></span>Tambah Homestay</a>
<br/>
<br/>

<?php 
$per_hal=10;
$jumlah_record=mysqli_query($koneksi, "SELECT * from homestay");
$jum=mysqli_num_rows($jumlah_record);
$halaman=ceil($jum / $per_hal);
$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
$start = ($page - 1) * $per_hal;
?>
<div class="col-md-12">
  <table class="col-md-2">
    <tr>
      <td>Jumlah Record</td>    
      <td><?php echo $jum; ?></td>
    </tr>
    <tr>
      <td>Jumlah Halaman</td> 
      <td><?php echo $halaman; ?></td>
    </tr>
  </table>
  <button = style="margin-bottom:10px" onClick="print_data()" class="btn btn-default pull-right"><span class='glyphicon glyphicon-print'></span>  Cetak</button> 
</div>
<form action="cari_produk.php" method="GET">
  <div class="input-group col-md-5 col-md-offset-7">
    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search"></span></span> 
    <input type="text" class="form-control" placeholder="Cari berdasarkan ID Homestay, nama dan jumlah di sini .." aria-describedby="basic-addon1" name="cari">
  </div>
</form><br />
  <?php 
  if(isset($_GET['cari'])){
    echo '<div> <b>Hasil pencarian dengan kata kunci "'. $_GET['cari'] .'"</b></div><br/>';
    $cari=mysqli_real_escape_string($koneksi, $_GET['cari']);
    $brg=mysqli_query($koneksi, "select * from homestay inner join kecamatan on homestay.id_kec = kecamatan.id_kec where nama like '%$cari%' or qty like '%$cari%' or id_homestay like '%$cari%'");
  }else{
    $brg=mysqli_query($koneksi, "select * from homestay inner join kecamatan on homestay.id_kec = kecamatan.id_kec order by nama DESC limit $start, $per_hal");
    
  }
  $no=1;
  $count = mysqli_num_rows($brg);
  if($count == null){
    if(isset($_GET['cari'])){
      echo '<div align="center"> <h5>Homestay dengan kata kunci "'. $_GET['cari'] .'" tidak ada. </h5> </div>';
    }
  }else{
  ?>
    <table class="table table-hover">
  <tr>
    <th class="col-md-1">No</th>
    <th class="col-md-1">ID Homestay</th>
    <th class="col-md-3">Nama Homestay</th>
    <th class="col-md-2">Kecamatan </th>
    <!-- <th class="col-md-2">Harga Jual</th> -->
    <th class="col-md-1">Jumlah Kamar</th>
    <th class="col-md-1">Fasilitas</th>
    <th class="col-md-1">Alamat</th>
    <th class="col-md-1">POI</th>
    <th class="col-md-2">Opsi</th>
  </tr>

  <?php
  while($b=mysqli_fetch_array($brg)){
    ?>
    <tr>
      <td><?php echo $no++ ?></td>
      <td><?php echo $b['id_homestay'] ?></td>
      <td><?php echo $b['nama'] ?></td>
      <td><?php echo $b['nama_kec']?></td>
      <!-- <td>Rp.<?php echo number_format($b['harga']) ?>,-</td> -->
      <td><?php echo $b['qty'] ?></td>
      <td><?php echo $b['fasilitas']?></td>
      <td><?php echo $b['alamat']?></td>
      <td><?php echo $b['poi']?></td>
      <td>
        <a href="det_homestay.php?id=<?php echo $b['id_homestay']; ?>" class="btn btn-info"><span class="glyphicon glyphicon-search"></span></a>
        <a href="edit.php?id=<?php echo $b['id_homestay']; ?>" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span></a>
        <a onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ??')){ location.href='hapus.php?id=<?php echo $b['id_homestay']; ?>' }" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
      </td>
    </tr>   
    <?php 
  }
  ?>
  <tr>
      
    
    </td>
  </tr>
</table>
<ul class="pagination">     
      <?php 
      for($x=1;$x<=$halaman;$x++){
        ?>
        <li><a href="?page=<?php echo $x ?>"><?php echo $x ?></a></li>
        <?php
      }
      ?>            
    </ul>
<?php
  }
  ?>
<script>
  function print_data() {
    <?php 
    if (isset($cari)) { ?>
      window.open("laporan_homestay.php?barang=<?php echo $cari?>","_blank");
    <?php }else{ ?>
      window.open("laporan_homestay.php","_blank");
    <?php } ?>
  }
</script>


                 <!-- /. ROW  -->
    <hr />
               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>